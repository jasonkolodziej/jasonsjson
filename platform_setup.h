
#ifndef PLATFORM_SETUP_H
#define PLATFORM_SETUP_H

#include <math.h>
#include <cmath>
#include <limits>
#include <float.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>


#if defined(_MSC_VER)

#include <windows.h>
#include <string>
//
typedef int64_t int_size;


// compatibility
namespace std
{

	
	//
	inline int isnan(double x)
	{
		return _isnan(x);
	}
	inline int isinf(double x)
	{
		return _finite(x) == 0;
	}

// apologies for the macros once more ...?
#define _IS_NAN(arg) std::isnan(arg)
#define _IS_INF(arg) std::isinf(arg)
#define __PLATFORM_THROW(arg) throw (arg)
}	

namespace JSON
{

class json_exception : public std::exception
{
public:

	json_exception(const char* arg) :
		std::exception(arg)
	{
	}
	
	// 
    virtual ~json_exception() throw()	{}

    // Returns a C-style character string describing 
	// the general cause of the current error.
	 virtual const char* what() const throw()
	 {
		return std::exception::what();
	 }
};	// class

}	//namespace

namespace Jerry
{

	
	// convert wide to ASCII
	inline std::string convert(const wchar_t* parg,int lcid = CP_UTF8)
	{
		std::string ret;
		size_t length = 0;
		if (parg)
		{
			length = wcslen(parg);
		}
		if (length)
		{
			// wide character string to multibyte character string 
#if (_MSC_VER >= 1400)	
			int chars = WideCharToMultiByte(lcid,0,parg,length,0,0,0,0);
			if (chars > 0)
			{
				ret.assign(chars,0);
				WideCharToMultiByte(lcid,0,parg,length,&ret[0],chars,0,0);
			}
#else		
			ret.assign(length,0);
			wcstombs(&ret[0],parg,length);
#endif		
		}
		return ret;
	}

	
	// convert wide to ASCII
	inline std::string convert(const std::wstring& warg,int lcid = CP_UTF8)
	{
		return convert(warg.c_str(),lcid);
	}

	
	// char to wchar_t
	inline std::wstring convert(const char* parg,int lcid = CP_UTF8)
	{
		std::wstring ret;
		size_t length = 0;

		if (parg)
		{
			length = strlen(parg);
		}
		if (length)
		{
			// multibyte to widechar
			int chars = MultiByteToWideChar(lcid,0,parg,length,0,0);
			if (chars > 0)
			{
				ret.assign(chars,0);
				MultiByteToWideChar(lcid,0,parg,length,&ret[0],chars);
			}
		}
		return ret;
	}

	
	// convert ASCII to wide
	inline std::wstring convert(const std::string& arg)
	{
		return convert(arg.c_str());
	}

}

#elif defined(__MACH__)
#include <stdlib.h>
#include <string>
#include <mach/clock.h>
#include <mach/mach.h>
#include <mach/mach_time.h>  // for mach_absolute_time() and friends



//
typedef int64_t int_size;

// apologies for the macros once more ...?
#define _IS_NAN(arg) isnan(arg)
#define _IS_INF(arg) isinf(arg)
#define __PLATFORM_THROW(arg) throw (arg)


// g++ ///WORK WITH ME
#elif defined(__GNUG__)

// apologies for the macros once more ...?
#define _IS_NAN(arg) std::isnan(arg)
#define _IS_INF(arg) std::isinf(arg)
#define __PLATFORM_THROW(arg) throw (arg)
//
typedef int64_t int_size;


#elif defined(__arm__)

//
// exceptions not enabled for embedded ARM code
//
// apologies for the macros once more ...?
#define _IS_NAN(arg) isnan(arg)
#define _IS_INF(arg) isinf(arg)
#define __PLATFORM_THROW(arg) throw (arg)
//
typedef int32_t int_size;


#include <string.h>
#include <stdlib.h>
#include <string>

namespace Jerry
{
    
		// Keil has no wcslen of its own ...
		static size_t wcslen(const wchar_t* parg)
		{
				size_t ret = 0;
				if (parg)
				{
						while (*parg++)
						{
								ret++;
						}
				}
				return ret;
		}
		
    
    // convert wide to ASCII
    inline std::string convert(const wchar_t* parg,int lcid = 0)
    {
        std::string ret;
        size_t length = 0;
        if (parg)
        {
            length = wcslen(parg);
        }
        if (length)
        {
            ret.assign(length,0);
            wcstombs(&ret[0],parg,length);
        }
        return ret;
    }
    
    
    // convert wide to ASCII
    inline std::string convert(const std::wstring& warg,int lcid = 0)
    {
        return convert(warg.c_str(),lcid);
    }
    
    
    // char to wchar_t
    inline std::wstring convert(const char* parg,int lcid = 0)
    {
        std::wstring ret;
        size_t length = 0;

        if (parg)
        {
            length = strlen(parg);
        }
        if (length)
        {
            ret.assign(length,0);
            mbstowcs(&ret[0],parg,ret.size());
        }
        return ret;
    }

    
    // convert ASCII to wide
    inline std::wstring convert(const std::string& arg)
    {
        return convert(arg.c_str());
    }

}
#else

#error No support for current platform

#endif //choose operating system

#if defined(__MACH__) || defined(__arm__)

#include <stdarg.h>

#endif

#if defined(__MACH__) || defined(__GNUG__)
namespace Jerry
{

    
    // convert wide to ASCII
    inline std::string convert(const wchar_t* parg,int lcid = 0)
    {
        std::string ret;
        size_t length = 0;
        if (parg)
        {
            length = wcslen(parg);
        }
        if (length)
        {
            ret.assign(length,0);
            wcstombs(&ret[0],parg,length);
        }
        return ret;
    }
    
    
    // convert wide to ASCII
    inline std::string convert(const std::wstring& warg,int lcid = 0)
    {
        return convert(warg.c_str(),lcid);
    }
    
    
    // char to wchar_t
    inline std::wstring convert(const char* parg,int lcid = 0)
    {
        std::wstring ret;
        size_t length = 0;

        if (parg)
        {
            length = strlen(parg);
        }
        if (length)
        {
            ret.assign(length,0);
            mbstowcs(&ret[0],parg,ret.size());
        }
        return ret;
    }

    
    // convert ASCII to wide
    inline std::wstring convert(const std::string& arg)
    {
        return convert(arg.c_str());
    }

}

namespace JSON
{

class json_exception : public std::exception
{
	// JME WTF: this means allocating off heap during a throw()
	std::string m_error;
	
public:
	json_exception(const char* arg) :
		m_error(arg == 0 ? "<null arg>" : arg)
	{
	}
	
	// 
    virtual ~json_exception() throw()	{}

    // Returns a C-style character string describing 
	// the general cause of the current error.
	 virtual const char* what() const throw()
	 {
		return m_error.c_str();
	 }
};	// class
}	//namespace

#endif
#endif //PLATFORM_SETUP_H